<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/login', 'App\Http\Controllers\Api\AuthController@login');
Route::group(['middleware' => ['token.verification']], function() {
    Route::post('/logout', 'App\Http\Controllers\Api\AuthController@logout');
    Route::post('/activities/get', 'App\Http\Controllers\Api\ActivitiesController@getActivities');
    Route::post('/activity/create', 'App\Http\Controllers\Api\ActivitiesController@createActivity');
    Route::post('/activity/delete', 'App\Http\Controllers\Api\ActivitiesController@deleteActivity');
    Route::post('/report/create', 'App\Http\Controllers\Api\ReportsController@createReport');
    Route::post('/send_email', 'App\Http\Controllers\Api\ReportsController@sendEmailWithReport');
});
