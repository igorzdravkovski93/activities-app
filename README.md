#  Activities App

Activities App is developed for creating daily activities and reports for those activities. The app was build with Laravel and React. 
The app is also deployed on heroku:  
- https://activities--app.herokuapp.com/

### Features
- Login feature where authentication is handled with Laravel Passport
- Create activity feature
- Delete Activity feature
- Create Activity report
- Create Print report
- Send report link and message on emal
- Logout feature


##  Installing / Getting started
Before you start, you must have installed php > 7.x, composer, npm, node.js and mysql database.

Clonnig the app:
```
git clone https://gitlab.com/igorzdravkovski93/activities-app.git
cd activities-app/

```

After you clone the app, to run the project locally, please run the following commands in your command line:

```
composer install (install composer dependencies)
cp .env.example .env (create .env file)
```
After .env file is created, edit the following variables in .env:

```
APP_NAME=Activities
APP_URL=http://localhost:8000

(optionally: copy the lines below to use my test email address, otherwise create your own email address)
MAIL_MAILER=smtp
MAIL_HOST=smtp.googlemail.com
MAIL_PORT=587
MAIL_USERNAME=no.reply.prolabo@gmail.com
MAIL_PASSWORD=xokpfbcfqofelhxz
MAIL_ENCRYPTION=tls
MAIL_FROM_ADDRESS=no.reply.prolabo@gmail.com
MAIL_FROM_NAME="${APP_NAME}"
```

Then, create a table in the database and edit the following variables in .env:
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=table_name
DB_USERNAME=mysql_user
DB_PASSWORD=mysql_password
```
After that, run the following commands:
```
php artisan key:generate (generate application key)
php artisan migrate (create tables in database)
php artisan db:seed (fill tables with data)
php artisan passport:install (install auth client)
php artisan storage:link (link storage with public folder)

npm install
npm run dev (build the react app)

php artisan serve (start laravel server)
```

Visit http://localhost:8000/login and log in with the following credentials:
```
email: admin@admin.com
password: 12345678
```
