<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ACTIVITIES</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->

        <link rel="icon" href="https://img.icons8.com/ios-glyphs/30/000000/activity.png" sizes="16x16">
    </head>
    <body>
        <div class="container-fluid main" id="root"></div>
    </body>
    <script>
            var APP_URL = '{{ url('/') }}';
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>
    <script defer type="text/javascript" src="../js/app.js"></script>
</html>
