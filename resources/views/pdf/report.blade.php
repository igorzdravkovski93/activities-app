<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            table {
                font-family: Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
                margin-top: 20px;
            }

            table td, table th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            table th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #009FB7;
                color: white;
            }

            #total_duration_text {
                font-weight: bold;
                text-align: right;
            }

            #total_duration {
                font-weight: bold;
                text-align: left;
            }

            header {
                position: fixed;
                top: -60px;
                left: 0px;
                right: 0px;
                height: 50px;

                color: #009FB7;
                text-align: left;
                line-height: 35px;
            }

            footer {
                position: fixed;
                bottom: -60px;
                left: 0px;
                right: 0px;
                height: 50px;

                color: #009FB7;
                text-align: center;
                line-height: 35px;
            }

            #title {
                padding: 20px 0;
            }
        </style>
    </head>
    <body>

        <header>
            <h2>Activities</h2>
        </header>

        <footer>
            Copyright &copy; Activities {{ date("Y") }}
        </footer>

        <h3 id="title">Report N: {{ $report_number }}</h3>

        @if ($activity_report === true)
        <table>
            <thead>
                <tr>
                    <th>#</th>
                    <th>Description</th>
                    <th>Duration</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                @foreach($activities as $activity)
                    <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ $activity['description'] }}</td>
                        <td>{{ substr($activity['activity_duration'], 0, 5) }}</td>
                        <td>{{ $activity['activity_date'] }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @endif

        @if ($activity_report === false)
            @foreach($activities as $group)
                <table>
                    <thead>
                        <tr>
                            <th colspan="3">
                                Activities on date: {{ $group[0]['activity_date'] }}
                            </th>
                        </tr>
                        <tr>
                            <th>#</th>
                            <th>Description</th>
                            <th>Duration</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($group as $activity)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $activity['description'] }}</td>
                                <td>{{ substr($activity['activity_duration'], 0, 5) }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="2" id="total_duration_text">Total activities duration:</td>
                            <td id="total_duration">{{ $activity_duration[$loop->index] }}</td>
                        </tr>
                    </tbody>
                </table>
            @endforeach
        @endif
    </body>
</html>
