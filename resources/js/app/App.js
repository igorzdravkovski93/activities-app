import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { persistStore, persistReducer } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
import storage from 'redux-persist/lib/storage';
import allReducers from './store/reducers/index';
import {
    BrowserRouter as Router,
    Routes,
    Route,
  } from "react-router-dom";

import { Row, Col } from 'reactstrap';

import Header from './components/Header';
import Footer from './components/Footer';
import Dashboard from './views/Dashboard';
import Login from './views/Login';
import Activity from './views/Activity';

import 'bootstrap/dist/css/bootstrap.min.css';
import "react-datepicker/dist/react-datepicker.css";


const persistConfig = {
    key: 'root',
    storage,
};

const persistedReducer = persistReducer(persistConfig, allReducers);

const store = createStore(
    persistedReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
const persistor = persistStore(store);

const loading = () => <div className="animated fadeIn pt-1 text-center text-white">Loading...</div>

const App = () =>  {
    return (
        <Router>
            <Row>
                <Col xs="12" style={{padding: 0}}>
                    <Header/>
                </Col>
            </Row>
            <Suspense fallback={() => loading()}>
                <Routes>
                    <Route
                        exact path="/login"
                        element={<Login />} />
                    <Route
                        path="/dashboard"
                        element={<Dashboard />} />
                    <Route
                        path="/activity/create"
                        element={<Activity />} />
                    <Route
                        path="*"
                        element={<Login />} />
                </Routes>
            </Suspense>
            <Row>
                <Col xs="12" style={{padding: 0}}>
                    <Footer/>
                </Col>
            </Row>
        </Router>
    )
};

export default App;

if (document.getElementById('root')) {
    ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <App />
        </PersistGate>
    </Provider>,
    document.getElementById('root'));
}
