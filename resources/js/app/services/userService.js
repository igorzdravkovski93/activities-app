const axios = require('axios').default;

class userService {

  static login (data) {
    return axios.post('/api/login', data)
  }

  static logout(token) {
    return axios.post('/api/logout', {}, { headers: {"Authorization" : `Bearer ${token}`} } )
  }
}

export default userService;
