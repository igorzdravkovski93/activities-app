const axios = require('axios').default;

class activityService {

  static getActivities (data, token) {
    return axios.post('/api/activities/get', data, { headers: {"Authorization" : `Bearer ${token}`} })
  }

  static createActivity(data, token) {
    return axios.post('/api/activity/create', data, { headers: {"Authorization" : `Bearer ${token}`} } )
  }

  static deleteActivity(data, token) {
    return axios.post('/api/activity/delete', data, { headers: {"Authorization" : `Bearer ${token}`} } )
  }
}

export default activityService;
