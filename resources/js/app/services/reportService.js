const axios = require('axios').default;

class reportService {

  static createReport (data, token) {
    return axios.post('/api/report/create', data, { headers: {"Authorization" : `Bearer ${token}`} })
  }

  static sendEmail (data, token) {
    return axios.post('/api/send_email', data, { headers: {"Authorization" : `Bearer ${token}`} })
  }
}

export default reportService;
