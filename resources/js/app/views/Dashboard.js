import React, { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import {
    Container,
    Col,
    Row,
    Button
} from 'reactstrap';


import ActivitiesTable from '../components/ActivitiesTable';
import LoadingSpinner from '../components/LoadingSpinner';
import AppPagination from '../components/Pagination';

import activityService from '../services/activityService';
import reportService from '../services/reportService';
import { logout } from '../store/actions/user';

import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';

const Dashboard = () => {

    const [activities, setActivities] = useState([]);
    const [pages, setNumberOfPages] = useState(0);
    const [requestParams, setRequestParams] = useState({
        offset: 0,
        sort: 'DESC',
        start_date: '1990-01-01',
        end_date: '2100-01-01'
    })
    const [loading, setLoading] = useState(true);
    const [rerender, setRerender] = useState(false);
    const user = useSelector(state => state.user);
    const navigate = useNavigate();
    const MySwal = withReactContent(Swal);
    const dispatch = useDispatch();

    // redirect user to /activity/create

    const handleCreateNewClick = () => {
        return navigate('/activity/create');
    }

    // handle print report and activity report buttons

    const handleReportBtnClick = (e) => {
        let button = e.currentTarget.id;

        // open modal with inputs for start and end date

        MySwal.fire({
            title: button === 'download' ? 'Download report' : 'Print report',
            html:
                `<div class="d-flex flex-column" style="text-align: left;">
                    <label for="date_1">Starting date</label>
                    <input id="date_1" type="date" class="swal2-input" style="margin: 2% 0">
                    <label for="date_2">Ending date</label>
                    <input id="date_2" type="date" class="swal2-input" style="margin: 2% 0">
                    <div id="error_message" class="text-danger"></div>
                </div>`,
            showCancelButton: true,
            confirmButtonText: button === 'download' ? '<div id="report_btn">Download</div>' : '<div id="report_btn">Create</div>',
            showLoaderOnConfirm: true,
            didOpen: () => {
                // submit form on Enter
                let btn = document.getElementById('report_btn').parentElement;
                document.querySelectorAll('.swal2-input').forEach(input => {
                    input.addEventListener('keypress', function(e) {
                        if(e.charCode === 13) {
                            btn.click()
                        }
                    })
                });
            },
            preConfirm: () => {

                // validation of the inputs

                let dates = {
                    date_1: document.getElementById('date_1').value,
                    date_2: document.getElementById('date_2').value,
                }

                let d1 = new Date(dates.date_1);
                let d2 = new Date(dates.date_2);

                let error_msg = '';

                if(d1 > d2) {
                    error_msg = 'Starting date can not be bigger than ending date';
                }

                Object.values(dates).map(dateValue => {
                    let inputDate = dateValue.split('-');
                    if(inputDate[0].length > 4) {
                        error_msg = 'Year must have 4 digits';
                    }

                    let date = new Date(dateValue)
                    if (isNaN(date.valueOf())) {
                        error_msg = 'Date format must be mm/dd/yyyy';
                    }
                })

                // if validation fails, show error message

                if(error_msg.length > 0) {
                    return MySwal.showValidationMessage(
                        error_msg
                    );
                }

                // if validation is ok, show loading spinner

                setLoading(true);

                // send request for print report or for activity report

                button === 'download' ? (
                    reportService.createReport(dates, user.token)
                    .then(response => {

                        // hide loading spinner
                        setLoading(false);

                        if(response.data.type === 'error') {
                            if(response.data.action === 'logout') {
                                // if token is not valid, logout user
                                dispatch(logout(user.token));
                                navigate('/login');
                            }
                        } else if (response.data.type === 'success'){
                            if(response.data.message) {
                                // if there are no activities in that time interval, show message
                                MySwal.fire({
                                    title: 'Info!',
                                    text: response.data.message,
                                    icon: 'info',
                                    showConfirmButton: true,
                                    showCloseButton: false,
                                });
                            } else {

                                // download file
                                const link = document.createElement('a');
                                link.href = window.location.origin + response.data.pdf;
                                link.setAttribute('download', response.data.file_name);
                                document.body.appendChild(link);
                                link.click();
                                link.parentNode.removeChild(link);
                            }
                        }
                    })
                    .catch(error => {
                        // catch error and show message
                        MySwal.fire({
                            title: 'Error!',
                            text: 'Something went wrong. Please try again later',
                            icon: 'error',
                            timer: 2000,
                            showConfirmButton: false,
                            showCloseButton: false,
                        });
                    })
                ) : (
                    reportService.createReport(dates, user.token)
                    .then(response => {

                        // hide loading spinner
                        setLoading(false);

                        if(response.data.type === 'error') {
                            if(response.data.action === 'logout') {
                                 // if token is not valid, logout user
                                dispatch(logout(user.token));
                                navigate('/login');
                            }
                        } else if (response.data.type === 'success'){
                            if(response.data.message) {
                                // if there are no activities in that time interval, show message
                                MySwal.fire({
                                    title: 'Info!',
                                    text: response.data.message,
                                    icon: 'info',
                                    showConfirmButton: true,
                                    showCloseButton: false,
                                });
                            } else {
                                // if report is created, open it in new tab
                                window.open(window.location.origin + response.data.pdf);
                            }
                        }
                    })
                    .catch(error => {
                        // catch error and show message
                        MySwal.fire({
                            title: 'Error!',
                            text: 'Something went wrong. Please try again later',
                            icon: 'error',
                            timer: 2000,
                            showConfirmButton: false,
                            showCloseButton: false,
                        });
                    })
                )

            },
        })
    }

    // handle email btn click

    const handleEmailBtnClick = () => {
        // show form
        MySwal.fire({
            title: 'Send email',
            html:
                `<div class="d-flex flex-column" style="text-align: left;">
                    <label for="date_1">Starting date</label>
                    <input id="date_1" type="date" class="swal2-input" style="margin: 2% 0">
                    <label for="date_2">Ending date</label>
                    <input id="date_2" type="date" class="swal2-input" style="margin: 2% 0">
                    <label for="email">Email</label>
                    <input id="email_msg" type="text" class="swal2-input" style="margin: 2% 0">
                    <label for="subject">Subject</label>
                    <input id="subject" type="text" class="swal2-input" style="margin: 2% 0">
                    <label for="message">Message</label>
                    <input id="message" type="textarea" class="swal2-input" style="margin: 2% 0">
                </div>`,
            showCancelButton: true,
            confirmButtonText: '<div id="send_email">Send</div>',
            showLoaderOnConfirm: true,
            didOpen: () => {
                // submit form on Enter
                let btn = document.getElementById('send_email').parentElement;
                document.querySelectorAll('.swal2-input').forEach(input => {
                    input.addEventListener('keypress', function(e) {
                        if(e.charCode === 13) {
                            btn.click()
                        }
                    });
                });
            },
            preConfirm: () => {

                // validate inputs

                let data = {
                    date_1: document.getElementById('date_1').value,
                    date_2: document.getElementById('date_2').value,
                    email: document.getElementById('email_msg').value,
                    subject: document.getElementById('subject').value,
                    message: document.getElementById('message').value
                };

                let error_msg = '';

                let d1 = new Date(data.date_1);
                let d2 = new Date(data.date_2);

                if(d1 > d2) {
                    error_msg = 'Starting date can not be bigger than ending date';
                }

                Object.entries(data).map(value => {
                    if(value[0] === 'date_1' || value[0] === 'date_2') {
                        let inputDate = value[1].split('-');
                        if(inputDate[0].length > 4) {
                            error_msg = 'Year must have 4 digits';
                        }

                        let date = new Date(value[1]);
                        if (isNaN(date.valueOf())) {
                            error_msg = 'Date format must be mm/dd/yyyy';
                        }
                    }

                    if(value[0] === 'email') {
                        if(!value[1].match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/)) {
                            error_msg = 'Please enter valid email address';
                        }
                    }

                    if(value[0] === 'subject') {
                        if(value[1].length < 5) {
                            error_msg = 'Subject must be at least 5 characters long';
                        }

                        if(value[1].length > 50) {
                            error_msg = 'Subject can have max 50 characters';
                        }

                    }

                    if(value[0] === 'message') {
                        if(value[1].length < 5) {
                            error_msg = 'Message must be at least 5 characters long';
                        }

                        if(value[1].length > 200) {
                            error_msg = 'Message can have max 200 characters';
                        }
                    }
                })

                // if validation fails, show message

                if(error_msg.length > 0) {
                    return MySwal.showValidationMessage(
                        error_msg
                    );
                }

                // show loading spinner

                setLoading(true);

                // send request for sending message

                reportService.sendEmail(data, user.token)
                .then(response => {

                    // hide loading spinner

                    setLoading(false);

                    if(response.data.type === 'error') {
                        if(response.data.action === 'logout') {
                            // if token is invalid, logout user
                            dispatch(logout(user.token));
                            navigate('/login');
                        }
                    } else if (response.data.type === 'success'){
                        if(response.data.message) {
                            // if email send show success message
                            MySwal.fire({
                                title: 'Info!',
                                text: response.data.message,
                                icon: 'info',
                                showConfirmButton: true,
                                showCloseButton: false,
                            });
                        } else {
                            // if there are no activities in that time interval, show message
                            MySwal.fire({
                                title: 'Info!',
                                text: response.data.message,
                                icon: 'info',
                                timer: 1500,
                                showConfirmButton: false,
                                showCloseButton: false,
                            });
                        }
                    }
                })
                .catch(error => {
                    // if error, catch it and show message
                    MySwal.fire({
                        title: 'Error!',
                        text: 'Something went wrong. Please try again later',
                        icon: 'error',
                        timer: 2000,
                        showConfirmButton: false,
                        showCloseButton: false,
                    });
                })


            },
        })

    }

    // when activity is deleted, rerender component

    const onDelete = (param) => {
        setRerender(param);
    }

    // send request to get the sorted activities

    const handleSortAction = () => {
        let params = {...requestParams};

        requestParams.sort === 'DESC' ?
            params['sort'] = 'ASC' :
            params['sort'] = 'DESC';

        setRequestParams(params);
        setLoading(true);
    }

    // send request when page is changed

    const handlePaginationClick = (page) => {
        let params = {...requestParams};

        params['offset'] = (page - 1) * 10;

        setRequestParams(params);
        setLoading(true);
    }

    // send request when filter is changed

    const setFilterParams = (dates) => {
        let params = {...requestParams};

        let date_1 = '1990-01-01';
        let date_2 = '2100-01-01';

        dates[0] !== null && dates[1] !== null ?
            date_1 = new Date(new Date(dates[0]).getTime() - (new Date(dates[0]).getTimezoneOffset() * 60000 ))
                        .toISOString()
                        .split("T")[0]
            : null;

        dates[1] !== null ?
            date_2= new Date(new Date(dates[1]).getTime() - (new Date(dates[1]).getTimezoneOffset() * 60000 ))
                        .toISOString()
                        .split("T")[0]
            : null;

        if(dates[0] !== null && dates[1] !== null) {
            params['start_date'] = date_1;
            params['end_date'] = date_2;
            params['offset'] = 0;

            setRequestParams(params);
            setLoading(true);
        }
    }

    // get activities when page loads or rerenders

    useEffect(() => {
        if(user) {
            activityService.getActivities(requestParams ,user.token)
                .then(response => {

                    if(response.data.type === 'error') {
                        if(response.data.action === 'logout') {
                            // if token is invalid, logout user
                            dispatch(logout(user.token));
                            navigate('/login');
                        }
                    } else {
                        // if there are no activities, hide loading spinner
                        if(response.data.data.length === 0) {
                            setLoading(false);
                        }

                        // set activities from response in state
                        setActivities(response.data.data);
                        setNumberOfPages(response.data.number_of_pages)
                    }
                })
                .catch(error => {

                    if(error.response.data.action == "logout") {
                        dispatch(logout(''));
                        navigate('/login', { replace: true });
                    }
                })
        }

    }, [rerender, requestParams])

    // if request for getting activities is not finished, show loading spinner
    useEffect(() => {
        if(activities.length > 0) {
            setLoading(false);
        }
    }, [activities])

    return (
        <Container style={styles.view}>
            {loading === false ? (
            <>
                <Row style={styles.title}>
                    <Col  md="9" xs="6">
                        <h3>Activities</h3>
                    </Col>
                    <Col md="3" xs="6" style={styles.buttonContainer}>
                        <Button style={styles.button}
                                onClick={handleCreateNewClick}>
                            <i className="fas fa-plus"></i>
                        </Button>
                        <Button style={styles.button}
                                onClick={handleReportBtnClick}
                                id="print">
                            <i className="fas fa-print"></i>
                        </Button>
                        <Button style={styles.button}
                                onClick={handleEmailBtnClick}
                                id="email">
                            <i className="far fa-envelope"></i>
                        </Button>
                        <Button style={styles.button}
                                onClick={handleReportBtnClick}
                                id="download">
                            <i className="fas fa-download"></i>
                        </Button>
                    </Col>
                </Row>
                <Row style={{padding: '10px'}}>
                    <Col md="12">
                        <ActivitiesTable
                            activities={activities}
                            onDelete={onDelete}
                            onSort={handleSortAction}
                            offset={requestParams.offset}
                            onFilter={setFilterParams}
                            />
                    </Col>
                </Row>
                {
                    pages > 0 ?
                        (
                            <Row>
                                <AppPagination
                                    numberOfPages={pages}
                                    activePage={requestParams.offset / 10}
                                    changePage={handlePaginationClick}
                                    />
                            </Row>
                        ) : null
                }
            </>)
            :
            <LoadingSpinner isActive={loading}/>
            }
        </Container>
    );
}

const styles = {
    view: {
        marginTop: '2%',
        marginBottom: '2%',
        borderRadius: '20px',
        minHeight: '100vh'
    },
    title: {
        color: '#009FB7',
        borderBottom: '3px solid rgb(255, 193, 7)',
        padding: '2.5% 1%',
    },
    buttonContainer: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    button: {
        backgroundColor: '#009FB7',
        marginLeft: '2%'
    }
}

export default Dashboard;
