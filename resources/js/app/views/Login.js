import React, { useState, useEffect } from 'react';
import {
    Form,
    FormGroup,
    Label,
    Input,
    Row,
    Col,
    Button
} from 'reactstrap'
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { login } from '../store/actions/user';
import userService from '../services/userService';

import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';

const Login = () => {

    const [loginData, setLoginData] = useState({email: '', password: ''});
    const [error, setError] = useState({email: '', password: ''});
    const navigate = useNavigate();
    const user = useSelector(state => state.user);
    const dispatch = useDispatch();
    const MySwal = withReactContent(Swal);

    useEffect(() => {
        // if user is logged in, redirect user to /dashboard
        if(user) {
            navigate('/dashboard');
        }
    },[])

    // handle change of inputs

    const handleInputChange = event => {
        let inputId = event.currentTarget.id;
        let inputValue = event.currentTarget.value;

        let errors = {...error};
        errors[inputId] = '';

        // validate inputs on change

        if(inputId === 'email') {
            if(!inputValue.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/)) {
                errors[inputId] = 'Please enter valid email address';
            }
        }

        if(inputId === 'password') {
            if(inputValue.length < 8) {
                errors[inputId] = 'Password must be at least 8 characters long';
            }

        }

        setError(errors);

        let newState = {...loginData};
        newState[inputId] = inputValue;
        setLoginData(newState);

    }



    // handle sending request for login

    const handleLogin = () => {
        // check if inputs passed validation
        let errors = Object.values(error).map(err => err === '');
        let inputLength = Object.values(loginData).map(param => param.length > 0);

        // if validation is passed
        if((errors[0] && errors[1]) && (inputLength[0] && inputLength[1])) {
            userService.login(loginData)
                .then(res => {
                    // if response is ok, save user token in redux state and redirect user to /dashboard
                    if(res.data.type == 'success') {
                        dispatch(login({
                            token: res.data.access_token
                        }));
                        navigate('/dashboard');
                    }
                })
                .catch(error => {
                    // if error, catch error and display it
                    MySwal.fire({
                        title: 'Error!',
                        text: error.response.data.message,
                        icon: 'error',
                        timer: 2000,
                        showConfirmButton: false,
                        showCloseButton: false,
                    });
                });
        }
    }

    // submit form on Enter
    const handleKeypress = e => {

      if (e.charCode === 13) {
        handleLogin();
      }
    };

    return (
        <Row
            style={styles.loginView}
            className="d-flex flex-column justify-content-center">
            <Col
                style={styles.loginContainer}
                xs={{size:8, offset: 2}}
                sm={{size:6, offset: 3}}
                md={{size:4, offset: 4}} >
                <div
                    style={styles.loginForm}
                    className="d-flex flex-column justify-content-center align-items-center">
                    <h1
                        className="text-center mb-4"
                        style={styles.text}>
                        Login
                    </h1>
                    <Form>
                        <FormGroup>
                            <Label
                                for="email"
                                style={styles.text}>
                                Email
                            </Label>
                            <Input
                                type="email"
                                id="email"
                                placeholder="Email"
                                onChange={handleInputChange}
                                onKeyPress={handleKeypress}
                                value={loginData.email}/>
                            <div
                                className="text-danger"
                                style={styles.errorMessage}>
                                {error.email}
                            </div>
                        </FormGroup>
                        <FormGroup>
                            <Label
                                for="password"
                                style={styles.text}>
                                Password
                            </Label>
                            <Input
                                type="password"
                                id="password"
                                placeholder="Password"
                                onChange={handleInputChange}
                                onKeyPress={handleKeypress}
                                value={loginData.password}/>
                            <div
                                className="text-danger"
                                style={styles.errorMessage}>
                                    {error.password}
                                </div>
                        </FormGroup>
                        <Button
                            style={styles.button}
                            onClick={handleLogin}
                            block>
                                Login
                        </Button>
                    </Form>
                </div>
            </Col>
        </Row>
    )
}

const styles = {
    loginView: {
        height: '100vh',
    },
    loginContainer: {
        padding: '10px',
        borderRadius: '20px',
    },
    loginForm: {
        backgroundColor: '#E6E6EA',
        padding: '30px',
        borderRadius: '20px',
        height: '100%',
        boxShadow: '5px 5px #F8F7FF'
    },
    text: {
        color: '#009FB7'
    },
    button: {
        background: '#009FB7',
        color: '#E6E6EA'
    },
    errorMessage: {
        fontSize: '10px',
        marginTop: '1%',
        height: '10px'
    },
}

export default Login;
