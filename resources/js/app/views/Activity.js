import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {
    Form,
    FormGroup,
    Label,
    Input,
    Row,
    Col,
    Button
} from 'reactstrap';

import activityService from '../services/activityService';
import { logout } from '../store/actions/user';

import LoadingSpinner from '../components/LoadingSpinner';

import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';


const Activity = () => {

    const [activityData, setActivityData] = useState({description: '', duration: '', date:''});
    const [error, setError] = useState({description: '', duration: '', date:''});
    const [btnDisabled, setBtnDisabled] = useState(true);
    const user = useSelector(state => state.user);
    const navigate = useNavigate();
    const MySwal = withReactContent(Swal);
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false);

    // handling change of inputs

    const handleInputChange = event => {
        let inputId = event.currentTarget.id;
        let inputValue = event.currentTarget.value;

        let errors = {...error};
        errors[inputId] = '';

        // validation of inputs on change

        if (inputId === 'duration') {
            if (!inputValue.match(/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/)) {
                errors[inputId] = '*Duration must be valid time format (hh:mm)';
            }
        }

        if(inputId === 'description') {

            if(inputValue.length < 10) {
                errors[inputId] = '*Description must be at least 10 characters long';
            }

            if(inputValue.length > 150) {
                errors[inputId] = '*Description can have max 100 characters';
            }
        }

        if(inputId === 'date') {
            let inputDate = inputValue.split('-')
            if(inputDate[0].length > 4) {
                errors[inputId] = '*Year must have 4 digits';
            }

            let date = new Date(inputValue)
            if (isNaN(date.valueOf())) {
                errors[inputId] = '*Date format must be mm/dd/yyyy';
            }
        }

        setError(errors);

        // set the new value of the input

        let newState = {...activityData};
        newState[inputId] = inputValue;
        setActivityData(newState);
    }

    // submitting data for creating activity

    const handleSubmit = () => {

        // check if inputs passed validation
        let errors = Object.values(error).map(err => err === '');
        let inputLength = Object.values(activityData).map(param => param.length > 0);

        let condition_1 = ((errors[0] && errors[1]) && (errors[2]));
        let condition_2 = (inputLength[0] && (inputLength[1] && inputLength[2]));

        // if validation is passed
        if(condition_1 && condition_2) {
            // show loading spinner
            setLoading(true);

            // send data to api

            activityService.createActivity(activityData, user.token).
                then(res => {
                    // hide loading spinner and retrieve response
                    setLoading(false)
                    if(res.data.type === 'error') {
                        if(res.data.action === 'logout') {
                            dispatch(logout(user.token));
                            navigate('/login')
                        }
                    } else {
                        MySwal.fire({
                            title: 'Success!',
                            text: res.data.message,
                            icon: 'success',
                            timer: 1500,
                            showConfirmButton: false,
                            showCloseButton: false,
                        }).then(() => {
                            navigate('/dashboard')
                        });
                    }
                })
                .catch(error => {
                    MySwal.fire({
                        title: 'Error!',
                        text: error.response.data.message,
                        icon: 'error',
                        timer: 2000,
                        showConfirmButton: false,
                        showCloseButton: false,
                    })
                });
        }
    }

    // submit form on Enter

    const handleKeypress = e => {

        if (e.charCode === 13) {
            handleSubmit();
        }
    };


    useEffect(() => {

        // if validation of the inputs fails, make create button disabled

        let disabled = false;

        // check if inputs have validation errors

        Object.values(error).map(err => {
            if(err !== '') {
                disabled = true
            }
        });

        // check if all inputs are filled

        Object.values(activityData).map(data => {
            if(data === '') {
                disabled = true
            }

        });

        setBtnDisabled(disabled);
    }, [handleInputChange])

    return (
        <Row style={styles.activityView} className="d-flex flex-column justify-content-center">
            {loading === false ? (
            <>
                <Col
                    style={styles.activityContainer}
                    xs={{size:8, offset: 2}}
                    sm={{size:6, offset: 3}} >
                    <div
                        style={styles.activityForm}
                        className="d-flex flex-column justify-content-center align-items-center">
                        <h2
                            className="mb-4"
                            style={styles.text}>
                            Create activity
                        </h2>
                        <Form style={styles.form}>
                            <FormGroup>
                                <Label
                                    for="description"
                                    style={styles.text}>
                                    Description
                                </Label>
                                <Input
                                    type="textarea"
                                    id="description"
                                    placeholder="Description"
                                    onChange={handleInputChange}
                                    onKeyPress={handleKeypress}
                                    value={activityData.description}/>
                                <div
                                    className="text-danger"
                                    style={styles.errorMessage}>
                                    {error.description}
                                </div>
                            </FormGroup>
                            <FormGroup>
                                <Label
                                    for="duration"
                                    style={styles.text}>
                                    Duration
                                </Label>
                                <Input
                                    type="text"
                                    id="duration"
                                    placeholder="Duration"
                                    onChange={handleInputChange}
                                    onKeyPress={handleKeypress}
                                    value={activityData.duration}/>
                                <div
                                    className="text-danger"
                                    style={styles.errorMessage}>
                                    {error.duration}
                                </div>
                            </FormGroup>
                            <FormGroup>
                                <Label
                                    for="date"
                                    style={styles.text}>
                                    Date
                                </Label>
                                <Input
                                    type="date"
                                    id="date"
                                    placeholder="Date"
                                    onChange={handleInputChange}
                                    onKeyPress={handleKeypress}
                                    value={activityData.date}/>
                                <div
                                    className="text-danger"
                                    style={styles.errorMessage}>
                                    {error.date}
                                </div>
                            </FormGroup>
                            <Button
                                style={styles.button}
                                onClick={handleSubmit}
                                block
                                disabled={btnDisabled}
                                >
                                    Create
                            </Button>
                        </Form>
                    </div>
                </Col>
            </>)
            :
            <LoadingSpinner isActive={loading}/>
            }
        </Row>
    )
}

const styles = {
    activityView: {
        height: '100vh',
    },
    activityContainer: {
        padding: '10px',
        borderRadius: '20px',
    },
    activityForm: {
        backgroundColor: '#E6E6EA',
        padding: '30px',
        borderRadius: '20px',
        height: '100%',
        boxShadow: '5px 5px #F8F7FF',
    },
    text: {
        color: '#009FB7'
    },
    button: {
        background: '#009FB7',
        color: '#E6E6EA'
    },
    errorMessage: {
        fontSize: '10px',
        marginTop: '1%',
        height: '10px'
    },
    form: {
        width: '100%'
    }
}

export default Activity;
