import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import activityService from '../services/activityService';
import { logout } from '../store/actions/user';


import { Table, Button } from 'reactstrap';
import DatePicker from "react-datepicker";
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';


import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';

const ActivitiesTable = ({ activities, onDelete, onSort, offset, onFilter }) => {

    const user = useSelector(state => state.user);
    const [deleteAction, setDeleteAction] = useState(0);
    const [sortDesc, setSortDesc] = useState(true);
    const MySwal = withReactContent(Swal);
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(null);
    const onChange = (dates) => {
        const [start, end] = dates;
        setStartDate(start);
        setEndDate(end);
        onFilter(dates);
    };

    const [dropdownOpen, setDropdownOpen] = useState(false);

    // handle activity delete
    const handleDelete = (event) => {
        let id = event.currentTarget.id;
        // open warning modal
        MySwal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            // if user confirms delete, proceed
            if (result.isConfirmed) {
                // send delete request
                activityService.deleteActivity({id: id}, user.token).
                then(res => {
                    if(res.data.type === 'error') {
                        if(res.data.action === 'logout') {
                            // if user is not authenticated, log user out
                            dispatch(logout(user.token));
                            navigate('/login');
                        }
                    } else {
                        // if delete is successful, show success message
                        MySwal.fire({
                            title: 'Success!',
                            text: res.data.message,
                            icon: 'success',
                            timer: 1500,
                            showConfirmButton: false,
                            showCloseButton: false,
                        }).then(() => {
                            onDelete(deleteAction)
                            setDeleteAction(deleteAction + 1)
                        });

                    }
                })
                .catch(error => {
                    // if error, catch it and show message
                    MySwal.fire({
                        title: 'Error!',
                        text: 'Something went wrong. Please try again later',
                        icon: 'error',
                        timer: 2000,
                        showConfirmButton: false,
                        showCloseButton: false,
                    });
                });
            }
          })
    }

    // send request for sorting the data

    const handleSortBtnClick = () => {
        setSortDesc(!sortDesc);
        onSort(sortDesc);
    }

    return (
        <Table responsive style={styles.table}>
            <thead>
                <tr>
                    <th style={styles.tableHeaderText}>
                        #
                    </th>
                    <th style={styles.tableHeaderText}>
                        Description
                    </th>
                    <th style={{...styles.tableHeaderText, ...styles.dateHeader}}
                    >

                        <Dropdown isOpen={dropdownOpen} toggle={() => setDropdownOpen(!dropdownOpen)}>
                            <DropdownToggle style={styles.headerBtn}>
                                <span style={styles.dateHeader}>
                                    <i className="fas fa-filter"></i>
                                    <span>Date</span>
                                </span>
                            </DropdownToggle>
                            <DropdownMenu style={{position: 'fixed'}}>
                                <DropdownItem header>
                                    <DatePicker
                                        selected={startDate}
                                        onChange={onChange}
                                        startDate={startDate}
                                        endDate={endDate}
                                        selectsRange
                                        inline
                                    />
                                </DropdownItem>
                            </DropdownMenu>
                        </Dropdown>
                        <Button
                            style={styles.headerBtn}
                            onClick={handleSortBtnClick}>
                                <i className="fas fa-sort"></i>
                        </Button>

                    </th>
                    <th style={styles.tableHeaderText}>
                        Duration
                    </th>
                    <th style={styles.tableHeaderText}>
                        Action
                    </th>
                </tr>
            </thead>
            <tbody style={styles.tableBody}>
            {activities.map((activity, index) => {
                return (
                    <tr key={activity.id}>
                        <th scope="row" style={styles.tableHeaderText}>
                            {offset + index + 1}
                        </th>
                        <td style={styles.tableData}>
                            {activity.description}
                        </td>
                        <td style={styles.tableData}>
                            {activity.activity_date}
                        </td>
                        <td style={styles.tableData}>
                            {activity.activity_duration.slice(0,5)}
                        </td>
                        <td>
                            <Button color="danger"
                                    id={activity.id}
                                    onClick={handleDelete}>
                                        <i className="far fa-trash-alt"></i>
                            </Button>
                        </td>
                    </tr>
                )
            })}
            </tbody>
        </Table>
    )
}

const styles = {
    table: {
        minHeight: '20vh'
    },
    tableBody: {
        borderTopColor: 'rgb(255, 193, 7)',
    },
    tableHeaderText: {
        color: '#009FB7',
    },
    tableData: {
        color: '#009FB7'
    },
    sortBtn: {
        cursor: 'pointer',
    },
    headerBtn: {
        backgroundColor: 'white',
        border: 0,
        fontWeight: 'bold',
        color: '#009FB7',
        paddingBottom: 0
    },
    dateHeader: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
}

export default ActivitiesTable;
