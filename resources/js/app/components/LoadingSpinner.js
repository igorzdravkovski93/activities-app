import React from 'react';
import { Container, Spinner } from 'reactstrap';
import LoadingOverlay from 'react-loading-overlay';

const LoadingSpinner = props => {
    return (
        <Container>
            <LoadingOverlay
                active={props.isActive}
                spinner={
                    <div style={styles.spinnerContainer}>
                        <Spinner
                            color="primary"
                            type="grow"
                        >
                        </Spinner>
                        <Spinner
                            color="primary"
                            type="grow"
                        >
                        </Spinner>
                        <Spinner
                            color="primary"
                            type="grow"
                        >
                        </Spinner>
                    </div>
                }
                >
            </LoadingOverlay>
        </Container>
    )
}

const styles = {
    spinnerContainer: {
        position: 'fixed',
        left: '50%',
        top: '50%',
        transform: 'translate(-50%, -50%)'
    }
}

export default LoadingSpinner;
