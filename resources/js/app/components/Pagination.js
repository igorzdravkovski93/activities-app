import React, { useState } from "react";
import { Pagination, PaginationItem, PaginationLink, Button } from "reactstrap";

const AppPagination = ({numberOfPages, activePage, changePage}) => {

    const [pageNumbers] = useState(Array.from({length: numberOfPages}, (_, i) => i + 1));

    return (
        <Pagination style={styles.paginationContainer}>
            { activePage > 0 ?
            <PaginationItem>
                <PaginationLink
                    previous
                    onClick={() => changePage(activePage)}
                />
            </PaginationItem>
            : null
            }
            {
                pageNumbers.map(page => {
                    return (
                        <PaginationItem
                            key={page}
                            active={activePage === page - 1 ? true : false}
                        >
                            <PaginationLink
                                onClick={() => changePage(page)}
                            >
                                    {page}
                            </PaginationLink>
                        </PaginationItem>
                    )
                })
            }
            { activePage + 1 !== numberOfPages ?
                <PaginationItem>
                    <PaginationLink
                        next
                        onClick={() => changePage(activePage + 2)}
                    />
                </PaginationItem>
                : null
            }
        </Pagination>
    );
}

const styles = {
    paginationContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
}

export default AppPagination;
