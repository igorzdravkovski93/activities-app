import React, { useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from "react-router-dom";
import {
    Navbar,
    NavbarBrand,
    Button
  } from 'reactstrap';

import { logout } from '../store/actions/user';
import { useLocation } from 'react-router-dom';
import userService from '../services/userService';

const Header = () => {

    const dispatch = useDispatch();
    const navigate = useNavigate();
    const user = useSelector(state => state.user);
    const location = useLocation();

    useEffect(() => {
        if(!user) {
            if(location.pathname !== '/login') {
                // if user is not authenticated, log out user

                handleLogout(user.token)
                dispatch(logout(''));
                return  navigate('/login', { replace: true });
            }
        }
    },[])

    // handle click on Logout button
    const handleLogout = () => {
        if(user) {
            userService.logout(user.token).
                    then(res => {
                        // logout user and redirect to /login
                        dispatch(logout(user.token));
                        navigate('/login', { replace: true });
                    })
                    .catch(error => {
                        // if error, catch it and display a message
                        MySwal.fire({
                            title: 'Error!',
                            text: 'Something went wrong. Please try again later',
                            icon: 'error',
                            timer: 2000,
                            showConfirmButton: false,
                            showCloseButton: false,
                        });
                    })
        }
        // navigate user to /login
        navigate('/login', { replace: true });
    }

    return (
      <>
        <Navbar
            style={styles.navbar}
            light expand="md">
            <NavbarBrand
                href="/dashboard"
                style={styles.text}>
                    Activities
            </NavbarBrand>
            {location.pathname !== '/login' && user ?
                <Button
                    style={styles.button}
                    onClick={handleLogout}>
                        Log out
                </Button>
                :
                null
            }
      </Navbar>
      </>
    );
}

const styles = {
  navbar: {
      background: '#E6E6EA',
      border: 'none'
  },
  text: {
    color: '#009FB7'
  },
  button: {
      background: '#009FB7',
      color: '#E6E6EA'
  }
}

export default Header;
