import React from 'react'

const Footer = () => {

    let date = new Date();

    return (
        <div
            className="text-center mt-2"
            style={styles.footer}>
                ACTIVITIES &copy; {date.getFullYear()}
        </div>
    )
}

const styles = {
    footer: {
        height: '40px',
        padding: '8px 0',
        background: '#E6E6EA',
        color: '#009FB7'
    },
  }

export default Footer;
