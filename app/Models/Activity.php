<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;

    protected $fillable = ['activity_date', 'activity_duration', 'description', 'user_id'];

    public function user()
    {
        return $this->user(Activity::class);
    }
}
