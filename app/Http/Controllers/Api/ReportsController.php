<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateReportRequest;
use App\Models\Activity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Report;
use PDF;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use App\Notifications\ReportNotification;

class ReportsController extends Controller
{


    // method for creating activity report
    public function createReport(CreateReportRequest $request)
    {

        // get user activities that match the time interval and group them by activity date
        $activities = Activity::where(function ($query) use ($request) {
                                return $query->where('user_id', auth()->guard('api')->user()->id)
                                            ->whereDate('activity_date', '>=', $request->date_1)
                                            ->whereDate('activity_date', '<=', $request->date_2);
                              })
                              ->orderBy('activity_date', 'DESC')
                              ->get()
                              ->groupBy('activity_date');

        // if there are no activities, return message that report can't be created
        if(count($activities) === 0) {
            return response()->json([
                'message' => "Please add activities in selected time interval before creating report",
                'type' => 'success'
            ], 200);
        }

        // calculate daily activities time for each day
        foreach($activities as $group) {
            $daily_activity_durations = [];
            foreach($group as $activity) {
                $daily_activity_durations[] = $activity->activity_duration;
            }

            $data['activity_duration'][] = $this->sumTime($daily_activity_durations);
        }

        // collect activities data for generating report
        $data['activities'] = $activities->toArray();
        $data['activity_report'] = false;

        $file_name = strtotime(now());
        $data['report_number'] = $file_name;

        // send data for creating report
        $report = PDF::loadView('pdf/report', $data);

        $data['report_number'] = $file_name;

        // delete all previous reports, except reports which link is send on email
        $this->deletePastReports();

        // store pdf file in storage
        Storage::disk('public')->put("pdf/$file_name.pdf", $report->output());

        // store storage pdf path in reports table
        Report::create(['report_path' => "/storage/pdf/$file_name.pdf"]);

        // return successful response
        return response()->json([
            'pdf' => "/storage/pdf/$file_name.pdf",
            'file_name' => "$file_name.pdf",
            'type' => 'success'
        ], 200);
    }

    // method for creating report and sending report link and message on email
    public function sendEmailWithReport(Request $request) {

        // get user activities that match the time interval
        $activities = Activity::where(function ($query) use ($request) {
                                    $query->where('user_id', auth()->guard('api')->user()->id)
                                        ->whereDate('activity_date', '>=', $request->date_1)
                                        ->whereDate('activity_date', '<=', $request->date_2);
                              })
                              ->orderBy('activity_date', 'DESC')
                              ->get();

        // if there are no activities, return message that report can't be created
        if(count($activities) === 0) {
            return response()->json([
                'message' => "Please add activities in selected time interval before creating report",
                'type' => 'success'
            ], 200);
        }

        // collect activities data for generating report
        $data['activities'] = $activities->toArray();
        $data['activity_report'] = true;

        $file_name = strtotime(now());
        $data['report_number'] = $file_name;

        // send data for creating report
        $report = PDF::loadView('pdf/report', $data);

        $data['report_number'] = $file_name;

        // delete all previous reports, except reports which link is send on email
        $this->deletePastReports();

         // store pdf file in storage
        Storage::disk('public')->put("pdf/$file_name.pdf", $report->output());

        // store storage pdf path in reports table
        Report::create([
            'report_path' => "/storage/pdf/$file_name.pdf",
            'send_by_email' => true
        ]);

        // prepare data for sending email
        $data = [
            'message' => $request->message,
            'subject' => $request->subject,
            'report_path' =>  config('app.url')."/storage/pdf/$file_name.pdf"
        ];

        // send email
        Notification::route('mail', $request->email)
                    ->route('data', $data)
                    ->notify(new ReportNotification($data));

        // return successful response
        return response()->json([
            'message' => 'Email successfully sent',
            'type' => 'success'
        ], 200);
    }

    // method that sums multiple time strings
    public function sumTime($times) {
        $minutes = 0;

        foreach ($times as $time) {
            list($hour, $minute) = explode(':', $time);
            $minutes += $hour * 60;
            $minutes += $minute;
        }

        $hours = floor($minutes / 60);
        $minutes -= $hours * 60;

        return sprintf('%02d:%02d', $hours, $minutes);
    }

    // method that deletes past reports, except reports which link is send on email
    public function deletePastReports()
    {
        $reports = Report::where('send_by_email', false)
                         ->get();

        if(count($reports) > 0) {
            foreach($reports as $report) {
                if (Storage::disk('public')->exists(str_replace('storage/', '',$report->report_path))) {
                    Storage::disk('public')->delete(str_replace('storage/', '',$report->report_path));
                }

                $report->delete();
            }
        }

        return true;
    }
}
