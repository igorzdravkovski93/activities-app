<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ActivitiesRequest;
use App\Http\Requests\CreateActivityRequest;
use App\Http\Requests\DeleteActivityRequest;
use App\Models\Activity;

class ActivitiesController extends Controller
{

    // method that returns all activities that are releated with authenticated user

    public function getActivities(ActivitiesRequest $request) {

        $activities = Activity::orderBy('activity_date', $request->sort)
                              ->where(function ($query) use ($request) {
                                  return $query->where('user_id', auth()->guard('api')->user()->id)
                                               ->whereDate('activity_date', '>=', $request->start_date)
                                               ->whereDate('activity_date', '<=', $request->end_date);
                              })
                              ->limit(10)
                              ->offset($request->offset)
                              ->get();
        $activities_count = Activity::orderBy('activity_date', $request->sort)
                                    ->where(function ($query) use ($request) {
                                        return $query->where('user_id', auth()->guard('api')->user()->id)
                                                    ->whereDate('activity_date', '>=', $request->start_date)
                                                    ->whereDate('activity_date', '<=', $request->end_date);
                                    })
                                    ->count();

        return response()->json([
            'data' => $activities,
            'number_of_pages' => ceil($activities_count / 10),
            'type' => 'success'
        ], 200);
    }

    // method that creates new activity for authenticated user

    public function createActivity(CreateActivityRequest $request) {

        // creating new activity with data from request

        Activity::create([
            'activity_date' => $request->date,
            'description' => $request->description,
            'activity_duration' => $request->duration,
            'user_id' => auth()->guard('api')->user()->id
        ]);

        // return successful response after creation of activity

        return response()->json([
            'message' => 'Activity successfully created',
            'type' => 'success'
        ], 200);
    }

     // method for deleting activity

    public function deleteActivity(DeleteActivityRequest $request) {

        // check if activity exists

        $activity = Activity::where('user_id', auth()->guard('api')->user()->id)
                            ->find($request->id);

        // if activity not exist, return error message

        if(!$activity) {
            return response()->json([
                "message" => 'Activity not found',
                'type' => 'error'
            ], 404);
        }

        // if activity exist, delete it and return success message

        $activity->delete();

        return response()->json([
            'message' => 'Activity successfully deleted',
            'type' => 'success'
        ], 200);
    }
}
