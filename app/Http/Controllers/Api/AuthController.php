<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    // method for logging user

    public function login(AuthRequest $request)
    {
        // lowercase email input

        $request->email = strtolower($request->email);

        // check if credentials exists in database

        if (!Auth::attempt($request->all())) {
            // if credentials not exists, return error message
            return response()->json([
                'message' => 'Credentials not exist',
                'type' => 'error'
            ], 404);
        }

        // create token for user

        $user = auth()->user();

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        // return success message

        return response()->json([
            'user' => $user,
            'access_token' => $accessToken,
            'type' => 'success'
        ], 200);
    }

    // method for logging user

    public function logout(Request $request)
    {
        // delete token from database
        $token = $request->token;
        $token->revoke();

        return response()->json(['type' => 'success'], 200);
    }
}
