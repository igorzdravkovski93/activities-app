<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => ['string','required','min:10','max:100'],
            'date' => ['required','date', 'date_format:Y-m-d'],
            'duration' => ['required', 'regex:/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/']
        ];
    }

    /**
     * Return error messages.
     *
     * @return array
     */

    public function response(array $errors)
    {
        return response()->json(['message' => $errors, 'type' => 'error'], 422);
    }
}
