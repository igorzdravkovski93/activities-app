<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeleteActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['integer','required','min:1'],
        ];
    }

    /**
     * Return error messages.
     *
     * @return array
     */

    public function response(array $errors)
    {
        return response()->json(['message' => $errors, 'type' => 'error'], 422);
    }
}
