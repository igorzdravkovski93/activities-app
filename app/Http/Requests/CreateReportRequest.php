<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_1' => ['required','date', 'date_format:Y-m-d'],
            'date_2' => ['required','date', 'date_format:Y-m-d', 'after_or_equal:date_1'],
        ];
    }

    /**
     * Return error messages.
     *
     * @return array
     */

    public function response(array $errors)
    {
        return response()->json(['message' => $errors, 'type' => 'error'], 422);
    }
}
