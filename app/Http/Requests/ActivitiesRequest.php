<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ActivitiesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'offset' => ['required', 'integer'],
            'sort' => ['required', 'string', Rule::in(['ASC', 'DESC'])],
            'start_date' => ['required', 'date', 'date_format:Y-m-d'],
            'end_date' => ['required', 'date', 'date_format:Y-m-d']
        ];
    }

    /**
     * Return error messages.
     *
     * @return array
     */

    public function response(array $errors)
    {
        return response()->json(['message' => $errors, 'type' => 'error'], 422);
    }
}
