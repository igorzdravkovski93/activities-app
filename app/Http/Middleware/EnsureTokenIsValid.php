<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Laravel\Passport\PersonalAccessTokenFactory;
use Carbon\Carbon;
use App\Http\Controllers\Api\AuthController;

class EnsureTokenIsValid extends PersonalAccessTokenFactory
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // find access token
        $access_token = explode(' ', $request->header('authorization'));
        $token = $this->findAccessToken(['access_token' => $access_token[1]]);
        $request->path() === 'api/logout' ? $request['token'] = $token : null;

        // if token is expired, delete it
        if(isset($token->expires_at)) {
            if(Carbon::parse($token->expires_at)->lessThan(now())) {
                $request['token'] = $token;

                $logout = new AuthController();
                return $logout->logout($request);
            }
        } else {
            // if token not exist, send response for logout
            return response()->json(['action' => 'logout', 'type' => 'error'], 401);
        }
        return $next($request);
    }
}
