<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Activity;
use Faker\Factory as Faker;

class ActivitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 30; $i++){
            $faker = Faker::create();
            $date = $faker->dateTimeBetween("+0 days", "+$i days");
            Activity::create([
                'activity_date' => $date,
                'activity_duration' => gmdate("H:i", rand(600, 30000)),
                'description' => $faker->text($maxNbChars = 100),
                'user_id' => 1
            ]);
        }
    }
}
